package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
// This application will function as an endpoint that will be used in handling http request.
@RestController
// will require all routes within the class to use the set endpoint as part of its route.
@RequestMapping("/greeting")
public class DiscussionApplication {


	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	// localhost:8080/hello
	@GetMapping("/hello")
	// Map a get request to the route "/hello" and invoke the method hello().
	public String hello() {
		return "Hello World!";
	}

	// Route with String Query
	// localhost:8080/hi?name=value
	@GetMapping("/hi")
	// "@RequestParam" annotation
	public String hi(@RequestParam(value = "name", defaultValue = "John") String name) {
		return String.format("Hi %s", name);
	}

	// Multiple parameters
	// localhost:8080/friend?name=value&friend=value
	@GetMapping("/friend")
	public String friend(@RequestParam(value = "name", defaultValue = "Joe") String name, @RequestParam(value = "friend", defaultValue = "Jane") String friend) {
		return String.format("Hello %s!, My name is %s ", name, friend);
	}

	// Route with path variables
	// Dynamic data is obtained directly from the url
	// localhost:8080/name
	@GetMapping("/hello/{name}")
	// "@PathVariable" allows us to extract data directly from the URL
	public String greetFriend(@PathVariable("name") String name) {
		return String.format("Nice to meet you %s!", name);
	}

	// ACTIVITY FOR SO9

	ArrayList<String> enrollees = new ArrayList<String>();

	@GetMapping("/enroll")
	public String enroll(@RequestParam(value = "user", defaultValue = "Joe") String user) {
		enrollees.add(user);
		return String.format("Thank you for Enrolling, %s!", user);
	}

	@GetMapping("/getEnrollees")
	public String Enrollees() {
		return String.format(String.valueOf(enrollees));
	}

	@GetMapping("/nameage")
	//localhost:8080/greeting/nameage?name=value&age=value
	public String nameage(@RequestParam(value = "name", defaultValue = "Joe") String name, @RequestParam(value = "age", defaultValue = "0") int age) {
		return String.format("Hello %s, My age is %d ", name, age);
	}

	@GetMapping("/courses/{id}")
	// "@PathVariable" allows us to extract data directly from the URL
	public String courses(@PathVariable("id") String id) {
		if(id.equals("java101")){
			return String.format("Java 101, MWF 8:00AM-11:00AM, PHP 3000.00");
		}
		else if (id.equals("sql101")){
			return String.format("SQL 101, TTH 1:00PM-04:00PM, PHP 2000.00");
		}
		else if(id.equals("javaee101")){
			return String.format("Java EE 101, MFW 1:00PM-04:00PM, PHP 3500.00");
		}
		else {
			return String.format("Course cannot be found");
		}
	}
}
